package me.benjoseph.branchcare

import android.app.Application
import me.benjoseph.branchcare.dagger.AppModule
import me.benjoseph.branchcare.dagger.ApplicationComponent
import me.benjoseph.branchcare.dagger.DaggerApplicationComponent
import me.benjoseph.branchcare.dagger.NavigationModule

class BranchCareApp : Application() {

    lateinit var applicationComponent : ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        applicationComponent = initDagger(this)
    }

    private fun initDagger(app: BranchCareApp): ApplicationComponent {
        return DaggerApplicationComponent.builder()
            .appModule(AppModule(app))
            .navigationModule(NavigationModule())
            .build()
    }
}