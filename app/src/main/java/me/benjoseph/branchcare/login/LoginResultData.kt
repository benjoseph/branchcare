package me.benjoseph.branchcare.login

import com.google.gson.annotations.SerializedName

data class LoginResultData(@SerializedName("auth_token") val authToken: String)