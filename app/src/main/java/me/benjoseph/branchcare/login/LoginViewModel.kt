package me.benjoseph.branchcare.login

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.benjoseph.branchcare.repository.BranchRepository

class LoginViewModel : ViewModel() {

    private val branchRepository: BranchRepository = BranchRepository()

    private val loginResultData: MutableLiveData<LoginResultData> = MutableLiveData()

    fun getLoginLiveData() = loginResultData

    @SuppressLint("CheckResult")
    fun login(username: String, password: String) {
        Log.d("ben", "login:in viewModel ")
        branchRepository.login(username, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                loginResultData.value = it
            }
    }
}