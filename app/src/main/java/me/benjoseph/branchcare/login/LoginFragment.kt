package me.benjoseph.branchcare.login

import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import me.benjoseph.branchcare.BranchCareApp
import me.benjoseph.branchcare.NavigationManager
import me.benjoseph.branchcare.R
import javax.inject.Inject

class LoginFragment : Fragment() {

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var loginButton: Button

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    @Inject
    lateinit var navigationManager: NavigationManager

    companion object {
        const val email = "mailtobenjoseph@gmail.com"
        fun newInstance(): LoginFragment {
            val args = Bundle()
            val fragment = LoginFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.application as BranchCareApp).applicationComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false);
        loginButton = view.findViewById(R.id.login_button)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginViewModel = ViewModelProvider(this, LoginViewModelFactory())
            .get(LoginViewModel::class.java)

        loginViewModel.getLoginLiveData().observe(this, Observer { result ->
            sharedPreferences.edit().putString("token", result.authToken).apply()
            navigationManager.onLoginSuccess()
        })

        loginButton.setOnClickListener {
            loginViewModel.login(email, email.reversed())
        }
    }
}