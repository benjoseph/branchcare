package me.benjoseph.branchcare.login

data class LoginRequestData(val username :String, val password : String)