package me.benjoseph.branchcare

import androidx.fragment.app.Fragment
import me.benjoseph.branchcare.messages.conversation.ConversationViewFragment
import me.benjoseph.branchcare.messages.list.MessageListFragment

class NavigationManager {

    interface FragmentNavigation {
        fun displayFragment(fragment: Fragment)
    }
    lateinit var fragmentNavigation: FragmentNavigation;

    fun setFragmentNavigationInstance(fragmentNavigation: FragmentNavigation) {
        this.fragmentNavigation = fragmentNavigation
    }

    fun onLoginSuccess() {
        fragmentNavigation.displayFragment(MessageListFragment.newInstance())
    }

    fun onConversationSelected(threadId : Int) {
        val fragment = ConversationViewFragment.newInstance(threadId)
        fragmentNavigation.displayFragment(fragment)
    }


}