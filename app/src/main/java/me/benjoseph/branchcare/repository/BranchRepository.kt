package me.benjoseph.branchcare.repository

import io.reactivex.Observable
import me.benjoseph.branchcare.login.LoginRequestData
import me.benjoseph.branchcare.login.LoginResultData
import me.benjoseph.branchcare.messages.MessageData
import me.benjoseph.branchcare.messages.conversation.SendMessageData

class BranchRepository {

    private val branchService = RestClient().getBranchService()

    fun login(username: String, password: String) : Observable<LoginResultData> {
        return branchService.login(LoginRequestData(username, password))
    }

    fun messages(authToken: String) : Observable<MutableList<MessageData>> {
        return branchService.getAllMessages(authToken)
    }

    fun postMessage(authToken: String, sendMessageData: SendMessageData ) : Observable<MessageData> {
        return branchService.postMessage(authToken, sendMessageData)
    }

}