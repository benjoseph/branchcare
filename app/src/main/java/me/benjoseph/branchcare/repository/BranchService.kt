package me.benjoseph.branchcare.repository

import io.reactivex.Observable
import me.benjoseph.branchcare.messages.conversation.SendMessageData
import me.benjoseph.branchcare.login.LoginRequestData
import me.benjoseph.branchcare.login.LoginResultData
import me.benjoseph.branchcare.messages.MessageData
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface BranchService {

    companion object {
        const val AUTH_HEADER_KEY = "X-Branch-Auth-Token"
    }

    @POST("api/login")
    fun login(@Body loginRequestData : LoginRequestData) : Observable<LoginResultData>

    @GET("api/messages")
    fun getAllMessages(@Header(AUTH_HEADER_KEY) authorization : String) : Observable<MutableList<MessageData>>

    @POST("api/messages")
    fun postMessage(@Header(AUTH_HEADER_KEY) authorization : String, @Body sendMessageData : SendMessageData) : Observable<MessageData>

    @POST("api/reset")
    fun reset(@Header(AUTH_HEADER_KEY) authorization : String) : Call<MessageData>
}