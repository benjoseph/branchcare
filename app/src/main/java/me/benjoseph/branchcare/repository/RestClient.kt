package me.benjoseph.branchcare.repository

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class RestClient {

    companion object {
        private const val BASE_URL = "https://android-messaging.branch.co/"
    }

    private val builder = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(getHttpClient())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())

    private val retrofit = builder.build()

    fun getBranchService(): BranchService {
        return retrofit.create(BranchService::class.java)
    }

    private fun getHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder().addInterceptor { chain ->
            chain.proceed(
                chain.request().newBuilder().header("Content-Type", "application/json").build()
            )
        }.addInterceptor(loggingInterceptor).build()
    }
}