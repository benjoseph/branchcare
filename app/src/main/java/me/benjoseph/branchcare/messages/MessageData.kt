package me.benjoseph.branchcare.messages

import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

data class MessageData(
    @SerializedName("id") val id: Int,
    @SerializedName("thread_id") val threadId: Int,
    @SerializedName("user_id") val userId: Int,
    @SerializedName("agent_id") val agentId: Int,
    @SerializedName("body") val body: String,
    @SerializedName("timestamp") val timestamp: String
) {
    fun getTime(): Date {
        return SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(timestamp)
    }
}