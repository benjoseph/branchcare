package me.benjoseph.branchcare.messages

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.benjoseph.branchcare.messages.conversation.SendMessageData
import me.benjoseph.branchcare.repository.BranchRepository
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class MessagesViewModel : ViewModel() {

    private val branchRepository: BranchRepository = BranchRepository()

    private val messagesLiveData = MutableLiveData<MutableList<MessageData>>()
    private val sendMessageLiveData = MutableLiveData<MessageData>()

    fun getMessagesLiveData() = messagesLiveData
    fun getSendMessageLiveData() = sendMessageLiveData

    private fun getMessages(token: String): Observable<MutableList<MessageData>> {
        return branchRepository.messages(token)
    }

    @SuppressLint("CheckResult")
    fun postMessage(token: String, sendMessageData: SendMessageData) {
        branchRepository.postMessage(token, sendMessageData)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                sendMessageLiveData.value = it
            }
    }

    @SuppressLint("CheckResult")
    fun getMessagesForThreadId(token: String, threadId: Int) {
        getMessages(token)
            .map { it.filter { it.threadId == threadId } }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                messagesLiveData.value = it as MutableList<MessageData>?
            }
    }


    @SuppressLint("CheckResult")
    fun getLatestMessagesForEachThread(token: String) {
        getMessages(token)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                val latestMessageForEachThread: HashMap<Int, MessageData> = HashMap()
                for (message in it) {
                    val threadId = message.threadId
                    if (latestMessageForEachThread.containsKey(threadId)) {
                        if ((latestMessageForEachThread[threadId] as MessageData).getTime()
                                .compareTo(message.getTime()) < 0
                        ) {
                            latestMessageForEachThread.put(threadId, message)
                        }
                    } else {
                        latestMessageForEachThread.put(threadId, message)
                    }
                }
                messagesLiveData.value = ArrayList(latestMessageForEachThread.values)
            }
    }
}
