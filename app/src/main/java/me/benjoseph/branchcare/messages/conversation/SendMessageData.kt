package me.benjoseph.branchcare.messages.conversation

import com.google.gson.annotations.SerializedName

data class SendMessageData(@SerializedName("thread_id") val threadId : Int,
                           @SerializedName("body")val body : String)