package me.benjoseph.branchcare.messages.conversation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import me.benjoseph.branchcare.R
import me.benjoseph.branchcare.messages.MessageData

class ConversationAdapter(private val messageList: MutableList<MessageData>) :
    RecyclerView.Adapter<ConversationAdapter.MessageViewHolder>() {

    inner class MessageViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val textView : TextView = view.findViewById(R.id.message_preview)
        val timestampView : TextView = view.findViewById(R.id.timestamp_view)
        val agentIdView : TextView = view.findViewById(R.id.agent_id_view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.message_list_item, parent, false)

        return MessageViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return messageList.size
    }

    fun addItem(messageData: MessageData) {
        messageList.add(messageData)
        notifyDataSetChanged()
    }
    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val body = messageList[position].body
        Log.d("ben", "rv item threadId: ${messageList[position].threadId} body: $body")
        holder.textView.text = messageList[position].body
        holder.timestampView.text = messageList[position].timestamp
        if(messageList[position].agentId == 0){
            holder.agentIdView.visibility = View.GONE
        } else {
            holder.agentIdView.text = messageList[position].agentId.toString()
        }
    }

}