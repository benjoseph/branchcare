package me.benjoseph.branchcare.messages.list

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import me.benjoseph.branchcare.BranchCareApp
import me.benjoseph.branchcare.NavigationManager
import me.benjoseph.branchcare.R
import me.benjoseph.branchcare.messages.MessagesViewModel
import me.benjoseph.branchcare.messages.MessagesViewModelFactory
import javax.inject.Inject


class MessageListFragment : Fragment() {

    private lateinit var messagesViewModel: MessagesViewModel
    private lateinit var recyclerView: RecyclerView

    @Inject
    lateinit var navigationManager: NavigationManager
    @Inject
    lateinit var sharedPreferences: SharedPreferences


    companion object {
        fun newInstance(): MessageListFragment {
            val args = Bundle()

            val fragment =
                MessageListFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.application as BranchCareApp).applicationComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_message_list, container, false);
        recyclerView = view.findViewById(R.id.message_recycler_view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        messagesViewModel = ViewModelProvider(
            this,
            MessagesViewModelFactory()
        ).get(MessagesViewModel::class.java)
        val token = sharedPreferences.getString("token", "")

        messagesViewModel.getMessagesLiveData().observe(this, Observer { result ->
            Log.d("ben", "on msg list received size: ${result.size} ")
            val adapter = MessageListAdapter(result, navigationManager)
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(activity)
            recyclerView.addItemDecoration(
                DividerItemDecoration(
                    recyclerView.context,
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter.notifyDataSetChanged()
        })

        if (token != null) {
            messagesViewModel.getLatestMessagesForEachThread(token)
        }
    }
}