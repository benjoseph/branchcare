package me.benjoseph.branchcare.messages.conversation

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import me.benjoseph.branchcare.BranchCareApp
import me.benjoseph.branchcare.NavigationManager
import me.benjoseph.branchcare.R
import me.benjoseph.branchcare.messages.MessagesViewModel
import me.benjoseph.branchcare.messages.MessagesViewModelFactory
import javax.inject.Inject

class ConversationViewFragment : Fragment() {

    private lateinit var messagesViewModel: MessagesViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var messageInputField: EditText
    private lateinit var messageSendButton: Button
    private var threadId: Int? = null

    @Inject lateinit var sharedPreferences: SharedPreferences
    @Inject lateinit var navigationManager: NavigationManager

    companion object {
        private const val THREAD_ID_KEY = "thread_id_key"
        fun newInstance(threadId : Int): ConversationViewFragment {
            val args = Bundle()
            args.putInt(THREAD_ID_KEY, threadId)
            val fragment =
                ConversationViewFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.application as BranchCareApp).applicationComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_conversation_view, container, false);
        recyclerView = view.findViewById(R.id.conversation_recycler_view)
        messageInputField = view.findViewById(R.id.input_field)
        messageSendButton = view.findViewById(R.id.send_button)
        threadId = arguments?.getInt(THREAD_ID_KEY)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var adapter : ConversationAdapter? = null
        messagesViewModel = ViewModelProvider(
            this,
            MessagesViewModelFactory()
        ).get(MessagesViewModel::class.java)
        val token = sharedPreferences.getString("token", "")

        messagesViewModel.getMessagesLiveData().observe(this, Observer { result ->
            result.sortBy { it.getTime() }
            adapter = ConversationAdapter(result)
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(activity)
            recyclerView.addItemDecoration(
                DividerItemDecoration(
                    recyclerView.context,
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter!!.notifyDataSetChanged()
        })
        messagesViewModel.getSendMessageLiveData().observe(this, Observer { result ->
            adapter?.addItem(result)
            messageInputField.text.clear()
        })


        if (token != null) {
            messagesViewModel.getMessagesForThreadId(token, threadId!!)
            messageSendButton.setOnClickListener {
                val sendMessageData = SendMessageData(threadId!!, messageInputField.text.toString())
                messagesViewModel.postMessage(token, sendMessageData)
            }
        }

    }
}