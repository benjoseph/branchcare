package me.benjoseph.branchcare.messages.list

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import me.benjoseph.branchcare.NavigationManager
import me.benjoseph.branchcare.R
import me.benjoseph.branchcare.messages.MessageData

class MessageListAdapter(
    private val messageList: List<MessageData>,
    private val navigationManager: NavigationManager
) :
    RecyclerView.Adapter<MessageListAdapter.MessageViewHolder>() {

    inner class MessageViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        val textView: TextView = view.findViewById(R.id.message_preview)

        init {
            view.setOnClickListener(View.OnClickListener {
                navigationManager.onConversationSelected(messageList[layoutPosition].threadId)
            })
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.message_list_item, parent, false)
        return MessageViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return messageList.size
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val body = messageList[position].body
        Log.d("ben", "rv item threadId: ${messageList[position].threadId} body: $body")
        holder.textView.text = messageList[position].body
    }

}