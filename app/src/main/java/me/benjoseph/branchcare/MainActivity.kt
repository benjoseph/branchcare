package me.benjoseph.branchcare

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import me.benjoseph.branchcare.login.LoginFragment
import javax.inject.Inject

class MainActivity : AppCompatActivity(), NavigationManager.FragmentNavigation {


    @Inject
    lateinit var navigationManager: NavigationManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as BranchCareApp).applicationComponent.inject(this)

        navigationManager.setFragmentNavigationInstance(this)
        displayFragment(LoginFragment.newInstance())
    }

    override fun displayFragment(fragment: Fragment) {
        val fragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager
            .beginTransaction()
        fragmentTransaction.replace(
            R.id.fragment_container,
            fragment
        ).addToBackStack(null).commit();
    }
}