package me.benjoseph.branchcare.dagger

import android.app.Activity
import dagger.Component
import me.benjoseph.branchcare.MainActivity
import me.benjoseph.branchcare.login.LoginFragment
import me.benjoseph.branchcare.messages.conversation.ConversationViewFragment
import me.benjoseph.branchcare.messages.list.MessageListFragment
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NavigationModule::class])
interface ApplicationComponent {

    fun inject(activity: MainActivity)

    fun inject(fragment: ConversationViewFragment)
    fun inject(fragment: MessageListFragment)
    fun inject(fragment: LoginFragment)
}