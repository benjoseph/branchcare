package me.benjoseph.branchcare.dagger

import dagger.Module
import dagger.Provides
import me.benjoseph.branchcare.NavigationManager
import javax.inject.Singleton

@Module
class NavigationModule {

    @Singleton
    @Provides
    fun providesNavigationManager() : NavigationManager {
        return NavigationManager()
    }
}