package me.benjoseph.branchcare.dagger

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {

    companion object {
        private const val SHARED_PREF_FILE_NAME = "default"
    }

    @Provides
    @Singleton
    fun providesApplication(): Application = application

    @Provides
    @Singleton
    fun providesContext(): Context = application

    @Provides
    @Singleton
    fun providesSharedPreferences(): SharedPreferences =
        application.getSharedPreferences(SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE)

}